IF OBJECT_ID('[dbo].[GetEmpSalary]') IS NOT NULL
	DROP PROCEDURE [dbo].[GetEmpSalary];

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[GetEmpSalary]( @empno int,@sal
numeric(7,2) output)AS SELECT @sal = sal from emp where empno = @empno;
GO
